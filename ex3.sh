#! /bin/bash
#ex2

echo "Enter A:"
read A
echo "Enter B:"
read B
echo "Enter C:"
read C

if [ $A -eq 0 ]
then
	echo "status: -1"
else
	delta=$((B*B-4*A*C))
	if [ $delta -lt 0 ]
	then
		echo "status: 0"
	elif [ $delta -eq 0 ]
	then 	
		x1=$((-b/(2*A)))
		printf "x1=x2=$x1"
		echo "status: 1"
	else
		z=`echo "scale=2;sqrt($delta)" | bc`
		x1=`echo "scale=2;(- $B+$z)/(2*$A)" | bc`
		x2=`echo "scale=2;(- $B-$z)/(2*$A)" | bc`
		printf "x1=$x1\nx2=$x2\n"
		echo "status: 2"
	fi
fi
